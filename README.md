# Udev Server Backupper

A simple backupper script that detects the desired hard drive, decrypts, mounts and creates a rsync backup of selected folders.

This is not a universal project: you have to tune it for your own usecase. It's being used on a company called "Enigma Serviços" in Brazil.

# License

The license is GPLv3. See license file for more details.
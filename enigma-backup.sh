#!/usr/bin/env bash
set -e
cryptoBlockName="ENIGMA-BACKUP"
localMountPoint="/mnt/arquivos"
backupMountPoint="/mnt/$cryptoBlockName"
diskUUID="10f6f784-4c26-4d87-b2d9-6645bf67666d"
partUUID="d4c1b2d4-fdf1-4a30-9b42-2898551263c6"

# Decrypt and mount
sleep 5
cryptsetup luksOpen /dev/disk/by-partuuid/"$partUUID" "$cryptoBlockName" --key-file /root/crypto_keyfile.bin
mount -o compress-force=zstd:3,autodefrag /dev/mapper/"$cryptoBlockName" "$backupMountPoint"

# Start Backup
date=`date "+%d-%b-%y-%R"`
logfile="/root/enigma-backup.txt"
for folder in Comercial Compras Diretoria Engenharia Financeiro Gerencia PCP Qualidade RH TI
do
        # Create folder if needed
        [ -d "$backupMountPoint"/"$folder" ] || {
                mkdir "$backupMountPoint"/"$folder"
                chmod 0666 "$backupMountPoint"/"$folder"/
        }

        # Create backup
        rsync --ignore-errors --quiet -rltv --chmod=D0777,F0666 --delete --log-file="$logfile" --exclude="/Histórico" "$localMountPoint"/"$folder"/ "$backupMountPoint"/"$folder"/
done

# Umount, close encryption and eject
umount "$backupMountPoint"
cryptsetup close "$cryptoBlockName"
sync
udisksctl power-off -b /dev/disk/by-uuid/"$diskUUID"
